# MONACO: Modes of Narration and Attribution Corpus

This corpus is constructed by the project group Modes of Narration and Attribution ([MONA](https://www.uni-goettingen.de/de/mona/626918.html)). We provide German literary texts annotated with three base phenomena: **Generalising Interpretation** (GI), **Comment**, and **Non-fictional Speech** (NfR), as well as **Attribution** on top of them.

## Annotation Guidelines

Our annotation guidelines are saved in `Annotationsrichtlinien.pdf` (in German). For English papers on the phenomena, see our [References](#references) section.

A quick reference is also provided here:

| Phenomenon     | Tag               | Short Explanation                                |
| -------------- | ----------------- | ------------------------------------------------ |
| GI             | ALL               | generalisation with overt universal quantifier   |
| GI             | BARE              | generalisation with covert generic quantifier    |
| GI             | DIV               | generalisation with overt vague quantifier       |
| GI             | EXIST             | generalisation with overt existential quantifier |
| GI             | MEIST             | generalisation with overt majority quantifier    |
| GI             | NEG               | generalisation with any quantifier and negation  |
| Comment        | Einstellung       | attitude to diegesis                             |
| Comment        | Interpretation    | interpretive comment                             |
| Comment        | Meta              | metanarrative or metafictional comment           |
| NfR            | Nichtfiktional    | non-fictional speech                             |
| NfR            | Nichtfiktional+mK | non-fictional speech + minimal reference context |

The phenomena are annotated on passage-level, where we define a passage as a sequence of clauses, i.e. one annotation span consists of at least one but possibly multiple clauses.

We provide gold standards for GI, Comment and NfR. Each gold-standard clause was annotated with Attribution by six annotators, which means that they attributed the clauses to one, several, or all possible speakers: *character*, *narrator*, *author*, and/or *work-external speaker*. Since we regard Attribution as subjective phenomenon, we do not create a gold standard but provide the individual annotations of the annotators.

## File Formats

Each text directory contains the following files:
- The plain text as `.txt` file.
- The gold standard annotation collection (`GGG.xml`) in CATMA format.
- The text and gold annotations in extended `.conllu` format.

Our extended CoNLL-U format uses the following columns:
- ID: Token index.
- FORM: Token form.
- LEMMA: Lemma of token form.
- UPOS: Universal part-of-speech tag.
- XPOS: Language-specific part-of-speech tag.
- FEATS: Morphological features.
- HEAD: Head of the current token.
- DEPREL: Universal dependency relation to the HEAD.
- DEPS: [not used]
- MISC: Index and grammatical features of the current clause.
- GI: List of gold GI tags.
- PGI: [reserved for predicted GI tags]
- COMMENT: List of gold Comment tags.
- PCOMMENT: [reserved for predicted Comment tags]
- NFR: List of gold NfR tags.
- PNFR: [reserved for predicted NfR tags]
- ATTR1: Annotated attribution for character.
- PATTR1: [reserved for predicted attribution for character]
- ATTR2: Annotated attribution for narrator.
- PATTR2: [reserved for predicted attribution for narrator]
- ATTR3: Annotated attribution for author.
- PATTR3: [reserved for predicted attribution for author]
- ATTR4: Annotated attribution for work-external speaker.
- PATTR4: [reserved for predicted attribution for work-external speaker]

Tokenisation, lemmatisation and parsing is performed with [spaCy](https://spacy.io/). For UD parsing, morphological analysis, clause-splitting and grammatical analysis, we use the methods from [here](https://gitlab.gwdg.de/tillmann.doenicke/tense-tagger).

## Column Format

We provide four CoNLL files for each text:

- `*_spaces.conllu`
- `*_spaces_feats.conllu`
- `*_tabs.conllu`
- `*_tabs_feats.conllu`

The `spaces` and `tabs` files use spaces (`\s\s+`) and tabs (`\t`) as column separator, respectively.
The `feats` files add morphological and grammatical features in columns `FEATS` and `MISC`, respectively:

- Morphological features are included for all tokens and use the format of the [Universal Features](https://universaldependencies.org/u/feat/) inventory.
- Grammatical features are included for verbs only and follow the clause index after a single space. If the verb is composite, the features are shown in the row of the main verb, whereas corresponding auxiliary and modal verbs only receive a `VerbType` feature.

The token-level tags for our phenomena have the following format:

- The index of the passage, succeeded by `:`. Indices are per-column.
- The tag of the passage as in the table above.
- (Optionally:) The ambiguity marker `[!]`. Sometimes, a passage is linguistically (i.e. syntactically, semantically, pragmatically, ...) ambiguous and it is not possible to assign a unique gold tag because of that. Such passages are marked with the ambiguity marker.

Several tags for the same token are separated by comma.

Attribution categories (character, narrator, author, work-external speaker) are encoded as follows:

- `1` indicates that an annotator assigned the category.
- `0` indicates that an annotator did not assign the category.
- `-` indicates that an annotator did not annotate here.

The annotations of the individual annotators are separated by `|`. Note that the annotations follow the same order of annotators for every text.

## Metadata

The file `annotation.csv` contains an overview of all annotated texts. The table has the following columns:
- Textname (*text name*): Name of the text; consisting of the author's last name and the title, separated by two underscores.
- Annotierenden-Kürzel (*collection name*): Name of the annotation collection.
- Gattung (*genre*): `Erzählung` (*story*), `Roman` (*novel*), or `Vers` (*epic poetry*).
- Perspektive (*perspective*): `first`, `second`, or `third`.
- Typ der Erzählinstanz (*type of narrator*): `heterodiegetisch` (*heterodiegetic*) or `homodiegetisch` (*homodiegetic*)
- Beteiligung der Erzählinstanz am Geschehen (*involvement of the narrator*): `beteiligt` (*involved*) or `unbeteiligt` (*uninvolved*)
- Tagset (*tagset*): Name of the tagset, including the tagset version.
- Phänomene (*phenomena*): List of the annotated phenomena.

## Lincence/Citation

This work is licensed under a Creative Commons Attribution (CC-BY) 4.0 International License. You should cite it as:

> Florian Barth, Tillmann Dönicke, Benjamin Gittel, Luisa Gödeke, Anna Mareike Hofmann, Anke Holler, Caroline Sporleder, and Hanna Varachkina (2021). "MONACO: Modes of Narration and Attribution Corpus". URL: https://gitlab.gwdg.de/mona/korpus-public

The texts in our corpus are partially affected by other licenses. The source for each text can be found in `korpus.csv`:

- `thalia.de ebook`: The text is not in the public domain. Therefore, we publish only annotations but not the plain text, and tokens are replaced by underscores in the `.conllu` files.

- `KOLIMO Gutenberg`: The text is included in KOLIMO and originates from Gutenberg-DE. The text is subject to the terms and conditions of use of [Gutenberg-DE](https://www.projekt-gutenberg.org/info/texte/info.html) which prohibits commercial use without permission. (That could be interpreted as CC-BY-NC-SA licence.)

- `KOLIMO textgrid`: The text is included in KOLIMO and originates from [The Digital Library in the TextGrid Repository](https://www.textgrid.de/digitale-bibliothek), which is licensed under a CC-BY 4.0 licence.

[KOLIMO](https://jberenike.github.io/dig_res.html) (Herrmann & Lauer, 2017) itself is licensed under a CC-BY license.

## References

Generalising Interpretation:

> Tillmann Dönicke, Luisa Gödeke, and Hanna Varachkina (2021). "Annotating Quantified Phenomena in Complex Sentence Structures Using the Example of Generalising Statements in Literary Texts". In Proceedings of the 17th Joint ACL - ISO Workshop on Interoperable Semantic Annotation.

Corpus:

> Berenike Herrmann and Gerhard Lauer (2017): "KOLIMO. A corpus of Literary Modernism for comparative analysis." URL: https://kolimo.uni-goettingen.de/about