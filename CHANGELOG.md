## Release Versions

Overview of the texts, number of annotated sentences, and annotated phenomena in each version.

### v2.0 (2021-12-09)

| Text                                                | Sentences | GI, Comment, NfR | Attr1, Attr2, Attr3, Attr4 |
| :-------------------------------------------------- | --------: | :--------------: | :------------------------: |
| Andreae__Die_chymische_Hochzeit                     |     218   |                ✓ |                          ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                     |     206   |                ✓ |                          ✓ |
| Fontane__Der_Stechlin                               |     375   |                ✓ |                          ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G    |     210   |                ✓ |                          ✓ |
| Goethe__Die_Wahlverwandtschaften                    |     687   |                ✓ |                          ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus   |     221   |                ✓ |                          ✓ |
| Hoffmann__Der_Sandmann                              |     203   |                ✓ |                          ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland |     228   |                ✓ |                          ✗ |
| Kafka__Der_Bau                                      |     207   |                ✓ |                          ✓ |
| Kleist__Michael_Kohlhaas                            |     241   |                ✓ |                          ✗ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim     |     217   |                ✓ |                          ✓ |
| Mann__Der_Zauberberg                                |     235   |                ✓ |                          ✗ |
| May__Winnetou_II                                    |     250   |                ✓ |                          ✗ |
| Musil__Der_Mann_ohne_Eigenschaften                  |     207   |                ✓ |                          ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                      |     230   |                ✓ |                          ✓ |
| Schnabel__Die_Insel_Felsenburg                      |     300   |                ✓ |                          ✓ |
| Seghers__Das_siebte_Kreuz                           |     220   |                ✓ |                          ✗ |
| Wieland__Geschichte_des_Agathon                     |     278   |                ✓ |                          ✓ |
| Zesen__Adriatische_Rosemund                         |     300   |                ✓ |                          ✓ |
| **19 texts**                                        | **5,033** |           **19** |                     **14** |

### v1.1 (2021-11-16)

| Text                                             | Sentences | GI, Comment, NfR | Attr1, Attr2, Attr3 |
| :----------------------------------------------- | --------: | :--------------: | :-----------------: |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                  |     200   |                ✓ |                   ✓ |
| Fontane__Der_Stechlin                            |     375   |                ✓ |                   ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G |     200   |                ✓ |                   ✓ |
| Goethe__Die_Wahlverwandtschaften                 |     686   |                ✓ |                   ✓ |
| Hoffmann__Der_Sandmann                           |     205   |                ✓ |                   ✓ |
| Kafka__Der_Bau                                   |     207   |                ✓ |                   ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim  |     205   |                ✓ |                   ✓ |
| Musil__Der_Mann_ohne_Eigenschaften               |     205   |                ✓ |                   ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                   |     231   |                ✓ |                   ✓ |
| Wieland__Geschichte_des_Agathon                  |     276   |                ✓ |                   ✓ |
| **10 texts**                                     | **2,790** |           **10** |              **10** |